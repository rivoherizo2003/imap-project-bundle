<?php
/**
 * Created by PhpStorm.
 * User: zo
 * Date: 7/9/21
 * Time: 2:43 PM
 */

namespace App\Utils;


class SearchMail
{
    protected $search;

    /**
     * @return mixed
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param mixed $search
     */
    public function setSearch($search): void
    {
        $this->search = $search;
    }
}