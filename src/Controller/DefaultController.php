<?php

namespace App\Controller;

use App\FormType\SearchMailType;
use App\Utils\SearchMail;
use Oz\ImapBundle\Service\OzImapService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
//    /**
//     * @var OzImapService
//     */
//    private $ozImapService;
//
//    public function __construct(OzImapService $ozImapService)
//    {
//        $this->ozImapService = $ozImapService;
//    }

    /**
     * @Route("/", methods={"GET","POST"}, name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(SearchMailType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
//            /** @var SearchMail $searchMail */
//            $searchMail = $form->getData();
//            $this->ozImapService->connect();
//            $mails = $this->ozImapService->getListEMail('SUBJECT '.$searchMail->getSearch());
//
//            return $this->render('base.html.twig', [
//                'form' => $form->createView(),
//                'emails' => $mails
//            ]);
        }

        return $this->render('base.html.twig', [
            'form' => $form->createView(),
            'emails' => [],
        ]);
    }
}
